# Tutorial_AR.js

**---CAT:**

Aquest tutorial proporciona una breu introducció a la llibreria ar.js de Jerome Etienne ( https://twitter.com/jerome_etienne ). 
Aquesta llibreria permet aconseguir Realitat Augmentada directament sobre el navegador web

Per accedir a les explicacions del tutorial: https://gitlab.com/webXR/Tutorial_AR.js/wikis/home

Trobareu més informació a la pàgina oficial de ar.js: https://github.com/jeromeetienne/ar.js

<hr>

**---ESP:**

Este tutorial proporciona una breve introducción a la librería ar.js de Jerome Etienne ( https://twitter.com/jerome_etienne ). 
Esta librería permite conseguir Realidad Aumentada directamente sobre el navegador web

Para acceder a las explicaciones del tutorial: https://gitlab.com/webxr/tutorial_ar.js/wikis/home/#es

Encontraréis más información en la página oficial de ar.js: https://github.com/jeromeetienne/ar.js

